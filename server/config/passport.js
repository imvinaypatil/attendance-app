const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');

const Users = mongoose.model('Users');

passport.use(new LocalStrategy({
  usernameField: 'user[uname]',
  passwordField: 'user[password]',
}, (uname, password, done) => {
  Users.findOne({ uname })
    .then((user) => {
      if(!user || !user.validatePassword(password)) {
        return done(null, false, { errors: { 'user name or password': 'is invalid' } });
      }

      return done(null, user);
    }).catch(done);
}));