const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../auth');
const Users = mongoose.model('Users');
const Students = mongoose.model('Students');
const Attendance = mongoose.model('Attendance');

router.post('/students', auth.required, (req, res, next) => {
    const { body: { student } } = req;
    if(!student.name) {
        return res.status(422).json({
          errors: {
            name: 'is required',
          },
        });
    }

    const finalStudent = new Students(student);
    finalStudent.save();
    return res.json(finalStudent);
});

router.get('/students', auth.required, (req, res, next) => {
    Students.find({}, function(err, students) {
        var studentsMap = [];
    
        students.forEach(function(student) {
            studentsMap.push(student);
        });
        res.send(studentsMap);  
      });
});

router.delete('/students/:student_id', auth.required, (req, res, next) => {
    const studentID = req.params.student_id;
    Students.deleteOne({ _id: studentID }, function (err, data) {
        if (err) {
            return res.status(422).json(err);
        }
        else return res.status(200).json(data);
    });
});

router.post('/students/:student_id', auth.required, (req, res, next) => {
    const studentID = req.params.student_id;
    const { body: { attendance } } = req;
    attendance.student = studentID;
    Students.findById(studentID).then((student) => {
        if(!student) {
            return res.sendStatus(400);
        }
        /* if isPresent then add db entry */
        if(attendance.isPresent) {
            const finalAttendance = new Attendance(attendance);
            finalAttendance.save(function(err) {
                if (err) {
                    return res.status(422).send(err);
                }
                return res.json(finalAttendance);
            });
        }
        /* if !isPresent then remove db entry */ 
        else {
            Attendance.deleteOne({ student: attendance.student, date: attendance.date }, function (err, data) {
                if (err) {
                    return res.status(422).json(err);
                }
                else return res.status(200).json(data);
            });
        }
    });
});

router.get('/students/:student_id', auth.required, (req, res, next) => {
    const studentID = req.params.student_id;
    Students.findById(studentID, function(err, student) {
        if(err || !student) {
            return res.status(404).json(err);
        }
        Attendance.find({student: student._id}, '-_id date isPresent').exec((err, attendance) => {
            if(err) return res.status(422).json(err);
            return res.status(200).json({
                student: student.name,
                attendance
            });
        });
    });
    return res.status(422);
});

module.exports = router;