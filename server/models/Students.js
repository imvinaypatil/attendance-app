const mongoose = require('mongoose');

const { Schema } = mongoose;

const StudentsSchema = new Schema({
  // roll_no: { type: Number, unique: true, required: true },
  name: String,
  attendances: [{ type: Schema.Types.ObjectId, ref: 'Attendance' }]
});

StudentsSchema.methods.setRollNo = function() {
  
}

const AttendanceSchema = new Schema({
    student: { type: Schema.Types.ObjectId, ref: 'Student' },
    date: { type: Date, default: Date.now, required: true },
    isPresent: { type: Boolean, default: true}
});

AttendanceSchema.index({ student: 1, date: 1}, { unique: true });

mongoose.model('Attendance', AttendanceSchema);
mongoose.model('Students', StudentsSchema);