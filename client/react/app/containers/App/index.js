/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import 'antd/dist/antd.css';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';

import LoginPage from 'containers/LoginPage/Loadable';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import { PrivateRoute } from '../../auth/_components/';

import GlobalStyle from '../../global-styles';
// setup backend
// import { configureBackend } from '../../auth/_helpers';
// configureBackend();

import Config from '../../config';
Config();

export default function App() {
  return (
    <div className="jumbotron">
        <div className="container">
                <Router>
                    <div>
                        <Route path="/login" component={LoginPage} />
                        <PrivateRoute exact path="/" component={HomePage} />
                    </div>
                </Router>
            </div>
        </div>
  );
}
