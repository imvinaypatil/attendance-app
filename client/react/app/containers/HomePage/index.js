/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon, DatePicker, Row, Col} from 'antd';
import moment from 'moment';

import StudentsTable from '../../components/StudentsTable';
import RegisterStudent from '../../components/RegisterStudent';
import { DateContext } from '../../components/date-conext';

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            date: new Date()
        };

        this.onDateSelected = this.onDateSelected.bind(this);
    }

    onDateSelected(date, dateString) {
        this.setState({
            date: dateString
        });
    }

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem('user'));
        console.log(user);
        if (!user) {
            // userService.logout();
        } else {
            this.setState({ 
                user: user,
            });
        }
    }

    render() {
        const dateFormat = 'YYYY-MM-DD';
        return (
            <div>
                <>
                    <Row>
                    <h1>Hello Master!</h1>
                    <p>
                        <Link to="/login">Logout</Link>
                    </p>
                    </Row>
                </>
                
                <>
                <DateContext.Provider value={this.state}>
                    <Row style= {{paddingBottom: 20}}>
                        <Col span={8}>
                        <DatePicker onChange={this.onDateSelected} defaultValue={moment(new Date(), dateFormat)} format={dateFormat} /> 
                        </Col>
                    </Row> 
                    <Row gutter={2}>
                        <Col span={14}>
                        <StudentsTable/>
                        </Col>
                        <div>
                            <Col span={4} offset={2}>
                                <RegisterStudent />
                            </Col>
                        </div>  
                    </Row>
                </DateContext.Provider>
                </>
            </div>
        );
    }
}

export { HomePage };