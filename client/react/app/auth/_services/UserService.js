import config from 'config';
import { authHeader } from '../_helpers';
import axios from 'axios';

function login(username, password) {
    const data = { user : {
        uname: username, password 
    }};
    return new Promise((resolve, reject) => {
        fetch('http://localhost:8086/api/users/login', {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        })
        .then((res) => {
            // login successful if there's a user in the response
            res.json().then((data) => {
                 const user = data.user;
                if (user) {
                    // store user details and basic auth credentials in local storage 
                    // to keep user logged in between page refreshes
                    user.username = username;
                    user.authdata = {Token: user.token};
                    localStorage.setItem('user', JSON.stringify(user));
                    resolve(user);
                }
                else if (res.status === 401 || 404 ) {
                    // auto logout if 401 response returned from api
                    logout();
                    location.reload(true);
                    reject(res);
                }
            })
        }).catch((err) => {
            reject(err);
        })
    })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

export const userService = {
    login,
    logout
};