import axios from 'axios';

export async function getStudentsDetail () {
    const data = [];
    let response = await axios.get('/api/admin/students');
    let i = 1;
    response.data.forEach((d) => {
        data.push({key: i, id: d._id, roll_no: i, name: d.name, attendances: d.attendances});
        i++;
    });
    return data;
}


