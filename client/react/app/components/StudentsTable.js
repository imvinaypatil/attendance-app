import React from 'react';
import {Table, Modal, Icon, Divider} from 'antd';
import { getStudentsDetail } from '../data';
import axios from 'axios';
import AttendanceCalendar from '../components/Calendar';
import { DateContext } from './date-conext';
import moment from 'moment';
const { Column, ColumnGroup } = Table;

class StudentsTable extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            calendarVisible: false,
            selectedStudent: {}
        }
    } 

    componentDidMount() {
        getStudentsDetail().then((data) => {
            this.setState({
                data: data
            });
        });
    }

    onViewAttendance(student) {
        this.setState({calendarVisible: true, selectedStudent : student});
    }
    
    onMarkPresent(student) {
        axios.post('/api/admin/students/'+student.id, {
            attendance: {
                date: new Date(this.context.date),
                isPresent: true
            }
        }).then((response) => {
            console.log(response.status);
        }).catch((err) => console.error(err.status));
    }

    onMarkAbsent(student) {
        axios.post('/api/admin/students/'+student.id, {
            attendance: {
                date: new Date(this.context.date),
                isPresent: false
            }
        }).then((response) => {
            console.log(response.status);
        }).catch((err) => console.error(err));
    }

    deleteStudent(student) {
        axios.delete('/api/admin/students/'+student.id)
            .catch((err) => console.log(err));
    }

    handleCalendarClose = (e) => {
        this.setState({calendarVisible: false});
    }

    getSelectedStudent() {
        return this.state.selectedStudent;
    }

    render() {
        return(
            <> 
                <Table style={{backgroundColor: '#E4E7EB', padding: 10}} dataSource={this.state.data}>
                
                    <Column
                        title = 'Roll No'
                        dataIndex = 'roll_no'
                        key = 'roll_no'
                        render = {text => <a href="javascript:;">{text}</a>}
                    />
                    <Column
                        title = 'Name'
                        dataIndex = 'name'
                        key = 'name'
                        render = {text => <a href="javascript:;">{text}</a>}
                    />
                    <Column
                    title = 'Action'
                    key = 'action'
                    render = {(text, record) => (
                      <span>
                        <a onClick={() => this.onViewAttendance(record)} style={{color: '#8FBCE6'}}>View Attendance</a>
                        <Divider type="vertical" />
                        <a onClick={() => this.onMarkPresent(record)} >Mark {name} as Present</a>
                        <Divider type="vertical" />
                        <a onClick={() => this.onMarkAbsent(record)} style={{color: '#D9822B'}}>Mark {name} as Absent</a>
                      </span>)}
                    />
                    <Column
                    title = 'Danger'
                    render = {(text, record) => <Icon onClick={() => this.onMarkAbsent(record)} style={{color: '#D9822B'}} type="delete" theme="outlined" />}
                    />
                </Table>
            
                <Modal title="Attendance sheet"
                    visible= {this.state.calendarVisible}
                    onOk={this.handleCalendarClose}
                    onCancel={this.handleCalendarClose}
                    >
                    <AttendanceCalendar student={this.getSelectedStudent()}></AttendanceCalendar>
                </Modal>
            </>
        );
    }
}
StudentsTable.contextType = DateContext;
export default StudentsTable;
  