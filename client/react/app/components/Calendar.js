import React from 'react';
import { Calendar, Icon, Button} from 'antd';
import moment from 'moment';
import axios from 'axios';

export default class AttendanceCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: props.visible || true,
            student: props.student,
            attendance: [],
            loading: false
        }

        this.dateCellRender = this.dateCellRender.bind(this);
        this.fetchData = this.fetchData(this);
        this.reload = this.reload(this);

        
    }

    componentDidMount() {
        axios.get('api/admin/students/'+this.state.student.id)
        .then((response) => {
            this.setState({attendance: response.data.attendance});
            // console.log(this.state.attendance);
        }).catch((err) => {
            console.error(err);
        })
    }

    fetchData() {
        
    }

    reload() {
        axios.get('api/admin/students/'+this.state.student.id)
        .then((response) => {
            this.setState({attendance: response.data.attendance});
        }).catch((err) => {
            console.error(err);
            this.setState({loading: true});
        })
    }

    shouldComponentUpdate() {
        return true;
    }

    onPanelChange(value, mode) {
        console.log(value, mode);
    }

    dateCellRender(date) {
        if (this.state.attendance[0]) {
            const dateFormat = 'YYYY-MM-DD';
            const filterPresent = this.state.attendance.filter((data) => data.isPresent && moment(data.date).isSame(date,"D"))
            return( filterPresent.length > 0 && <Icon type="star" theme="filled" />);
        }
    
    }

    render() {
        return(
            <>
                {/* <Button primry onClick={this.reload}>
                    <Icon type="reload" spin={this.state.loading} />
                </Button> */}
                <Calendar 
                    fullscreen={false} default={moment(new Date())} 
                    visible={this.state.visible} onPanelChange={this.onPanelChange}
                    dateCellRender={this.dateCellRender}
                    />
            </>
        );
    }
}