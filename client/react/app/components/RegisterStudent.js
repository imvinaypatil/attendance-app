import React from 'react';
import { Button, Modal, Input, Icon, Alert } from 'antd';
import axios from 'axios';

export default class RegisterStudent extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            ModalText: 'Enter Details',
            visible: false,
            confirmLoading: false,
            studentEntry: {}
        }
    }

    showModal = () => {
        this.setState({
            visible: true,
            ModalText: 'Enter Details',
        });
    }

    handleOk = (e) => {
        this.setState({
            ModalText: '',
            confirmLoading: true,
        });
        axios.post('/api/admin/students',{student: this.state.studentEntry})
            .then((response) => {
                this.setState({
                    visible: false,
                    confirmLoading: false,
                    studentEntry: {
                        name: ''
                    }
                });
                location.reload(true);
            }).catch((err) => {
                this.setState({
                    visible: false,
                    confirmLoading: false,
                    studentEntry: {
                        name: ''
                    }
                  });
            });
        
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }
    emitEmpty = () => {
        this.userNameInput.focus();
        this.setState({ studentEntry: {
            name: ''
        }});
    }
    render() {
        const { visible, confirmLoading, ModalText, studentEntry } = this.state;
        const suffix = studentEntry.name ? <Icon type="close-circle" onClick={this.emitEmpty} /> : null;
        return (
            <div>
                <Button primary onClick={this.showModal}>Register new Student</Button>
                <Modal title={ModalText}
                    visible={visible}
                    onOk={this.handleOk}
                    confirmLoading={confirmLoading}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>Return</Button>,
                        <Button key="submit" type="primary" loading={confirmLoading} onClick={this.handleOk}>
                          Submit
                        </Button>,
                      ]}
                    >
                    <p>{ModalText}</p>
                    <Input
                        placeholder="Enter Name"
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        suffix={suffix}
                        value={studentEntry.name}
                        onChange={(e) => this.setState({studentEntry: {
                            name: e.target.value
                        }})}
                        ref={node => this.userNameInput = node}
                    />
                </Modal>
            </div>
            
        );
    }
}