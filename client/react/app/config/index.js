import axios from 'axios';

// const AUTH_TOKEN = 'Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmFtZSI6InZpbmF5IiwiaWQiOiI1YmVhNzNhN2ZlMjZhNDcxZmZmMjdjZmQiLCJleHAiOjE1NDcyNzU2ODcsImlhdCI6MTU0MjA5MTY4N30.nHwv8IONH9aAr_DU5xQOpedspEWq_yrNJpBo4OXVgkk';
export default function configure() {
    axios.defaults.baseURL = 'http://127.0.0.1:8086';
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.authdata) {
        axios.defaults.headers.common['Authorization'] = 'Token '+user.authdata.Token;
    }
}
