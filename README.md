README

# Requirements 

- mongodb running on default port 
- nodejs with npm

# Instructions

- cd server/ & npm install
- cd client/react/ & npm install & npm build.
- cd server/
- npm start. 

![client/doc/Screenshot](client/doc/Screenshot from 2018-11-17 20-53-49.png)

